﻿using System;
using RabbitMQ.Client;
using System.Text;
using RabbitMQ.Wrapper;
using System.Threading;
using RabbitMQ.Client.Events;

namespace Ponger
{
    class Ponger
    {
        static RabbitMQWrapper rabbit;
        static void Main(string[] args)
        {
            MessageProducerSettings producerSettings = new MessageProducerSettings
            { ExchangeName = "Ping_Pong_Exchange", RoutingKey = "pong",ExchangeType="direct" };
            MessageConsumerSettings consumerSettings = new MessageConsumerSettings
            { ExchangeName = "Ping_Pong_Exchange", RoutingKey = "ping", QueueName = "pong" };
            rabbit = new RabbitMQWrapper(consumerSettings,producerSettings);
            Console.WriteLine("Ponger");
            rabbit.ListenMessageToQueue(MessageRecieve);

        }
        public static void MessageRecieve(object sender, BasicDeliverEventArgs ea)
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Receive {message} - {DateTime.Now}");
            Thread.Sleep(2500);
            rabbit.SendMessageToQueue("ping");
        }
    }
}
