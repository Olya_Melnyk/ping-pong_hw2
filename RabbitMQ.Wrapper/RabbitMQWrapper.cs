﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading;
namespace RabbitMQ.Wrapper
{
    public class RabbitMQWrapper
    {
        public MessageConsumerSettings consumerSettings;
        public MessageProducerSettings producerSettings;
        public static Lazy<IQueueService> queueServiceLazy;
        EventingBasicConsumer consumer;
        public IQueueService QueueService { get; set; }

        public RabbitMQWrapper(MessageConsumerSettings consumerSet,MessageProducerSettings producerSet)
        {
            consumerSettings = consumerSet;
            producerSettings = producerSet;
         
            queueServiceLazy = new Lazy<IQueueService>(() => new QueueService());
            QueueService = (QueueService)queueServiceLazy.Value;
            consumer = new EventingBasicConsumer(QueueService.Model);

            ApplySettings();
        }
        public void ApplySettings()
        {
            QueueService.ExchangeDeclare(consumerSettings.ExchangeName, producerSettings.ExchangeType);
            QueueService.QueueBind(consumerSettings);
        }
        public void SendMessageToQueue(string mes)
        {
            QueueService.Model.BasicPublish(
                exchange: producerSettings.ExchangeName,
                routingKey: producerSettings.RoutingKey,
                basicProperties: null,
                body: Encoding.UTF8.GetBytes(mes));
            Console.WriteLine($"Sent {mes} - {DateTime.Now}");
        }
        public void ListenMessageToQueue(EventHandler<BasicDeliverEventArgs> eventHandler)
        { 
            consumer.Received += eventHandler;
            QueueService.Model.BasicConsume(queue: consumerSettings.QueueName,
                                     autoAck: true,
                                     consumer: consumer);
            Console.ReadLine();
        }
        private IQueueService CreateQueueService()
        {
            return new QueueService();
        }
      
    }
}
