﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using Microsoft.Extensions.Configuration;
using System.Globalization;

namespace RabbitMQ.Wrapper
{
    public class QueueService:IQueueService
    {

        public IModel Model { get; set; }
        public IConnection connection { get; set; }
        public QueueService()
        {
            connection = new ConnectionFactory { HostName = "localhost" }
           .CreateConnection();
            Model = connection.CreateModel();
        }

        public void ExchangeDeclare(string exchangeName,string exchangeType)
        {
            Model.ExchangeDeclare(
                exchange: exchangeName,
                type: ExchangeType.Direct);
        }
        public void QueueBind(MessageConsumerSettings settings)
        {
            Model.QueueDeclare(
                    queue: settings.QueueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false);
            Model.QueueBind(queue: settings.QueueName,
                              exchange: settings.ExchangeName,
                              routingKey: settings.RoutingKey);
        }
        
        public void Dispose()
        {
            connection.Dispose();
            Model.Dispose();
        }
    }
}
