﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public class MessageConsumerSettings
    {
        public string ExchangeName { get; set; }
        public string QueueName { get; set; }
        public string RoutingKey { get; set; }
    }
}
