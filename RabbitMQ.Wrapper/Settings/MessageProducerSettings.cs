﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public class MessageProducerSettings
    {
        public string ExchangeName { get; set; }
        public string RoutingKey { get; set; }
        public string ExchangeType { get; set; }
    }
}
