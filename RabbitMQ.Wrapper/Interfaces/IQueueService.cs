﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public interface IQueueService: IDisposable
    {
        public  void ExchangeDeclare(string exchangeName, string exchangeType) { }
        public void QueueBind(MessageConsumerSettings settings) { }
        public IModel Model { get; set; }
    }
}
