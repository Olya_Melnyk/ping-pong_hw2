﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using RabbitMQ.Wrapper;
using System.Threading;
namespace Pinger
{
    class Pinger
    {
        public static RabbitMQWrapper rabbit;
        static void Main(string[] args)
        {
            MessageProducerSettings producerSettings=new MessageProducerSettings
            { ExchangeName = "Ping_Pong_Exchange", RoutingKey = "ping", ExchangeType="direct" };
            MessageConsumerSettings consumerSettings = new MessageConsumerSettings
            { ExchangeName = "Ping_Pong_Exchange", RoutingKey = "pong", QueueName = "ping" };
            rabbit = new RabbitMQWrapper(consumerSettings, producerSettings);
            Console.WriteLine("Pinger");
            rabbit.ListenMessageToQueue(MessageRecieve);
            rabbit.SendMessageToQueue("pong");

        }
        public static void MessageRecieve(object sender, BasicDeliverEventArgs ea)
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Receive {message} - {DateTime.Now}");
            Thread.Sleep(2500);
            rabbit.SendMessageToQueue("pong");
        }
    }
}
